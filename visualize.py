import signal
from signal import SIGTERM, SIGINT, SIGQUIT
from time import monotonic, sleep
import sap
import os
from traceback import format_exc
import sys
from pprint import pprint
import subprocess
from typing import Iterable, Mapping, Any, Tuple
from pathlib import Path

def quit_app(*args):
    exit(0)

def godot(
    initial_states: Mapping[str, Any],
    sig_set=(SIGTERM, SIGINT, SIGQUIT),
    timeout=5,
) -> subprocess.CompletedProcess:
    proc = subprocess.Popen(
        " ".join(( 
            "/Applications/Godot.app/Contents/MacOS/Godot",
            "--windowed", 
            "--resolution 720x720",
            "--path '/Volumes/Storage/Godot/Physics Simulation Visualization'",
            "--",
            "--ids '{}'".format(initial_states),
        )),
        text=True,
        shell=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    signal.sigwait(sig_set)
    # sleep(7)
    proc.terminate()
    while proc.poll() is None:
        sleep((1/4))
    Path("/Volumes/Storage/Godot/Physics Simulation Visualization/logs/stderr.log").write_text(proc.stderr.read())
    Path("/Volumes/Storage/Godot/Physics Simulation Visualization/logs/stdout.log").write_text(proc.stdout.read())

def test(sig_set):
    for sig in sig_set:
        signal.signal(sig, quit_app)

    sub = sap.Subscriber(
        "physics_update",
        quit_sig_set=sig_set,
    )
    
    first = True
    datum = monotonic()
    while True:
        try:
            sub.get_message()
            # if first or monotonic() - datum > 5.0: #print once every second
            #     os.system("clear")
            #     pprint(sub.get_message())
            #     datum = monotonic()
            # else:
            #     sub.get_message()
        except TimeoutError:
            pass
        except:
            print("Error Getting Message:\n\t{}".format(format_exc().replace("\n","\n\t")), file=sys.stderr)
        
        if first:
            first = False
