
from json import dumps
from multiprocessing.context import Process
from collections import UserDict
from typing import Iterable, Mapping, Tuple, Any
from multiprocessing.connection import Connection
from multiprocessing import RLock
from threading import Thread
from socket import create_server, socket, timeout as socket_timeout_error
from ipaddress import IPv4Interface
import json
import signal

class TopicData(UserDict):
    def __init__(self, topics: Iterable[str], initial_data : Mapping[str, Any]=None) -> None:
        data = {topic: {"lock": RLock(), "data": None} for topic in topics}
        if initial_data:
            for t, d in initial_data.items():
                if t not in topics:
                    raise KeyError(t)
                else:
                    data[t]["data"] = d

        super().__init__(data)
    
    def as_json(self, topic: str, timeout=1) -> str:
        if self[topic]["lock"].acquire(timeout=timeout):
            try:
                if self[topic]["data"]:
                    return json.dumps(self[topic]["data"])
                else:
                    raise ValueError(topic, None)         
            finally:
                self[topic]["lock"].release()
        else:
            raise TimeoutError("Failed to acquire lock after {}s".format(timeout))
    
    def update(self, topic: str, data: Any, timeout=1) -> None:
        assert (json_data := json.dumps(data)), "data is not JSON Encodable!"
        if self[topic]["lock"].acquire(timeout=timeout):
            try:
                self[topic]["data"] = json_data    
            finally:
                self[topic]["lock"].release()
        else:
            raise TimeoutError("Failed to acquire lock after {}s".format(timeout))

def pipe_message_broker(
    publishers: Iterable[Tuple[str, Connection]],
    subscribers: Iterable[Tuple[str, Connection]],
    sig_set: Iterable,
) -> None:
    topics = set((p[0] for p in publishers)) | set((s[0] for s in subscribers))
    time_to_quit = False

    def quit(*args):
        global time_to_quit 
        time_to_quit= True

    def topic_handler(
        topic: str,
        pub: Iterable[Connection], 
        sub: Iterable[Connection]
    ) -> int:
        msg_count = 0
        pub = tuple(pub)
        sub = tuple(sub)
        if len(pub) == len(sub) == 0:
            print("thread terminated: publisher/subcribers missing for topic '{}'".format(topic))
            return
        #Reads in values from publishers & sends to subscribers
        while not time_to_quit:
            for p in pub:               
                if p.poll():
                    try:
                        msg = p.recv()
                        print("Message Recieved on '{}'".format(topic))
                    except Exception as e:
                        print("Error Recieving on '{}': {}".format(topic, e))
                        continue

                    for s in sub:
                        try:
                            s.send(msg)
                            print("Message Sent on '{}'".format(topic))
                            msg_count += 1
                        except ValueError:
                            print("{}: oversized message".format(topic))
                        except Exception as e:
                            print("Error Sending on '{}': {}".format(topic, e))
                            continue
        
        print("terminating handler for '{}'".format(topic))
        return msg_count

    topic_threads = {}
    for topic in topics:
        print("Starting Handler for {}".format(topic))
        topic_threads[topic] = Thread(
            target=topic_handler,
            args=(
                topic,
                (p[1] for p in publishers if p[0] == topic),
                (s[1] for s in subscribers if s[0] == topic),
            )
        )

    for t in topic_threads.values():
        t.start()

    for sig in sig_set:
        signal.signal(sig, quit)

    for t in topic_threads.values():
        t.join()

def pipe_multiprocessing(
    publishers: Iterable[Tuple[str, Connection]],
    subscribers: Iterable[Tuple[str, Connection]],
    sig_set: Iterable,
) -> None:
    output = {
        "process_object": Process(
            target=pipe_message_broker,
            args=(
                tuple(publishers),
                tuple(subscribers),
                tuple(sig_set),
            )
        )
    }
    return output