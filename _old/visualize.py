from multiprocessing.connection import Connection
from typing import  Iterable, Mapping, Union, Any
from multiprocessing import Process
from time import monotonic

class Update:
    def __init__(
        self,
        update_delta: float,
        objs: Mapping[str, Any],
        physics_request: Connection,
        physics_states: Connection,
    ) -> None:
        self.update_delta = update_delta
        self.obs = objs
        self.physics_request = physics_request
        self.physics_states = physics_states
        self.ready_for_update = True
        self.update_recieved = True
        self.datum = monotonic()

    def update(self) -> None:
        if self.ready_for_update and self.update_recieved:
            print("Sending Physics Update Request")
            self.physics_request.send("physics_update")
            self.ready_for_update = False
            self.update_recieved = False

        if self.physics_states.poll():
            physics_data = self.physics_states.recv()
            self.update_recieved = True
            print("Physics Update Recieved")
            for obj_id in physics_data:
                try:
                    self.objs[obj_id].position = physics_data[obj_id]["pos"].as_tuple()
                except KeyError:
                    print("Key error")
        
        if monotonic() - self.datum >= self.update_delta:
            if self.update_recieved:
                self.ready_for_update = True
            self.datum = monotonic()

def generate_entities(ids: Iterable[Union[str, int]]) -> Mapping[Union[str, int], Any]:
    from ursina import Entity, color, Vec3
    return {id: Entity(model="sphere", color=color.random_color(), scale=Vec3(0.1,0.1,0.1)) for id in ids}

def visualize(
    ids: Iterable[Union[str, int]], 
    fps: float, 
    physics_request: Connection, 
    physics_states: Connection,
) -> None:
    from ursina import Ursina, camera, window
    delta = (1/fps)
    if delta < (1/60):
        print("WARNING! You're updating at a high rate, the update loop is not garunteed to match a framerate of {:2f} fps".format(fps))
    objs = generate_entities(ids)

    assert callable(
        update_step := Update(
            delta,
            objs,
            physics_request,
            physics_states,
        ).update
    )
    app = Ursina(update=update_step)
    window.fullscreen = True
    camera.orthagraphic = True
    camera.position = (0,0,-10)
    app.run()
    print("quiting visualization")

def multiprocess(
    ids: Iterable[Union[str, int]],
    fps: float,
    physics_request_sub: Connection,
    physics_update_pub: Connection,
) -> Mapping[str, Union[Connection, Process]]:

    output = {
        "process_object": Process(
            target=visualize,
            args=(
                ids,
                fps,
                physics_request_sub,
                physics_update_pub,
            ),
        )
    }

    return output
