"""String Application Protocol

Abbreviated SAP and pronounced like the sap from trees.

Built ontop of TCP, this is a simple protocol for facillitating Inter Proccess Communication.

It is designed to be a Publish/Subscribe Messaging System.

It is neither performant nor resilient; it is meant to be used for prototyping & proofs of concept.

Unlike most Systems you must preemptively define the topics the broker will accept.
Since this is an IPC drop-in the developer should know beforehand what topics they will use.

Packets consist of a header & data. Both are encoded as JSON strings.

Headers
--------

List of `Valid JSON Data Types <https://en.wikipedia.org/wiki/JSON#Data_types_and_syntax>`_.

.. code-block:: json

    {
        "client" : {
            "type": "(char[3]) Signifies the type of client as a zero padded string",
            "id": "(char[36]) UUID of the client"
        },
        "size": "(char[19]) The size of payload in bytes as a zero padded string.",
        "timestamp": "(char[19]) timestamp in microseconds since the connection with the Message Broker began as a zero padded string",
        "version": {
            "major": "(char[3]) The Major Version of the protocol as a zero padded string",
            "minor: "(char[3]) The Minor Version of the protocol as a zero padded string",
            "revision: "(char[3]) The Revision Version of the protocol as a zero padded string"
        }
    }

A list of potential `client_type`s include:

    :0: A Message Broker; this is the server which brokers messages on behalf of clients.
    :1: A Publisher; this Client exclusively publishes messages to a broker.
    :2: A Subscriber; this Client exclusively recieves messages from a broker.

Lifecycle
-----------

The lifecyle of a Subscribing Client is as follows:

    1. The subscribing client sends an initial packet informing the message broker it is a subscriber. `byte_size` is 0.
    2. The subscribing client sends a second packet informing the message broker of the topic it is subscribing too.
        A client can only subscribe to a single topic per connection instance.
    3. Then starts the main loop
        1. The Subscribing Client waits for packets from the Broker.
        2. The Message Broker sends the subscribing client a packet whenever a new message arives on a subscribed topic.
    4. When the Subscribing Client is finished it closes the connection with the Message Broker

The lifecycle of a Publishing Client is as follows.

    1. The Publishing client sends an initial packet informing the message broker it is a subscriber. `byte_size` is 0.
    2. The Publishing client sends a second packet informing the message broker of the topic it is publishing too.
        A client can only publish to a single topic per connection instance.
    3. Then starts the main loop
        1. The Message Broker waits for packets from the Publishing Client.
        2. The Publishing Client sends the Message Broker packets whenever new data is generated.
    4. When the Publishing Client is finished it closes the connection with the Message Broker.

In either case the Message Broker may close the connection to a client without warning, forcefully ending the lifecycle.
This can signify:

    1. A packet arrived that was in error.
    2. A fatal error occured on the Broker.
    3. The Message Broker was instructred to terminate.

"""
from __future__ import annotations
from collections import UserDict
from json import dumps, load
import multiprocessing
import queue
from struct import pack
from sys import stderr
from time import monotonic, time
from tkinter import Pack
from typing import Any, Mapping, Tuple, Iterable, Union
from uuid import UUID, uuid4
from ipaddress import IPv4Address, IPv4Interface
from socket import create_server, create_connection, socket, timeout as socket_timeout
from signal import signal, SIGQUIT, SIGTERM, SIGINT
from threading import Thread
from multiprocessing import RLock
from traceback import format_exc
import json
import sys

CLIENT_TYPES = {
    "message_broker": 0,
    "subscriber": 1,
    "publisher": 2,
}

HEADER_META = {
    "size": 204, #len == 204, sys.getsizeof == 237 of header_json_string.encode()
    "version": {
        "major": 0,
        "minor": 1,
        "revision": 0,
    }
}

def packet_header(
    client_type: int,
    client_id: Union[str, UUID],
    payload_size: int,
    timestamp: float=None,
    version_info: Mapping[str, int]=HEADER_META["version"],
) -> Mapping[str, Any]:
    """Creates a JSON header.
    """
    if timestamp is None:
        timestamp = time()
    return {
            "client": {
                "type": "{:03d}".format(int(client_type)),
                "id": str(client_id),
            },
            "size": "{:019d}".format(int(payload_size)),
            "timestamp": "{:019d}".format(int(timestamp*(10**6))), 
            "version": {
                "major": "{:03d}".format(int(version_info["major"])),
                "minor": "{:03d}".format(int(version_info["minor"])),
                "revision": "{:03d}".format(int(version_info["revision"])),
            }
        }

def decode_packet_header(json_data: str) -> Mapping[str, Any]:
    """Converts a json header into a python map with proper data types.
    """
    # print("decode_packet_header::TRACE::'{}'".format(json_data), file=sys.stderr)
    header = json.loads(json_data)
    return {
        "client": {
            "type": int(header["client"]["type"]),
            "id": UUID(header["client"]["id"])
        },
        "size": int(header["size"]),
        "timestamp": int(header["timestamp"]) / (10**6),
        "version":{
            "major": int(header["version"]["major"]),
            "minor":int(header["version"]["minor"]),
            "revision":int(header["version"]["revision"]),
        },
    }

class SAPError(Exception):
    pass

class FatalError(SAPError):
    pass

class InvalidTopic(SAPError):
    pass

class InvalidClientType(SAPError):
    pass

class OldPacket(UserDict):
    """A SAP packet consists of a JSON encoded header & a JSON encoded payload.

    You can access the native python values of the header keys like you would a python dictionary.

    These are all the keys found in the header.
    .. code-block:: json

        {
            "client" : {
                "type": "(uint8) Signifies the type of client",
                "id": "(char[36]) UUID of the client"
            },
            "size": "(uint64) Size of payload in bytes",
            "timestamp": "(float32) Fractional timestamp in seconds since the connection with the Message Broker began.",
            "version": {
                "major": "(uint8) The Major Version of the protocol",
                "minor: "(uint8) The Minor Version of the protocol",
                "revision: "(uint8) The Revision Version of the protocol"
            }
        }
    
    You can access the native python object payload via the "payload" key.
    In the event the "size" value is 0 then a Packet will not include a "payload" key.
    """
    def __init__(
        self,
        client_type: Union[str, int],
        client_id: UUID,
        timestamp: float,
        payload: Any=None,
        payload_size: int=0,
        version: Tuple[int,int,int]=(1,0,0),
    ) -> None:
        """Create A Packet

        Args:
            client_type (Union[str, int]): The type of client either as a string or int
            client_id (UUID): UUID of a client
            timestamp (float): time since epoch
            payload (Any): data the packet contains
            payload_size (int, optional): Override the payload size value if the passed payload is None. Defaults to 0.
            version (Tuple[int,int,int], optional): SAP version. Defaults to (1,0,0).
        """
        assert isinstance(client_type, (str, int))
        assert isinstance(client_id, UUID)
        assert isinstance(timestamp, float)
        assert isinstance(version, type(()))

        if payload is not None:
            if isinstance(payload, _JSONEncodable):
                self._payload = payload
            else:
                self._payload = _JSONEncodable(payload)
        if isinstance(client_type, str):
            self._client_type = CLIENT_TYPES[client_type]
        else:
            self._client_type = client_type
        self._client_id = client_id
        self._timestamp = timestamp
        self._major_version = version[0]
        self._minor_version = version[1]
        self._revision_version = version[2]

        _data = {
            "client": {
                "type": self._client_type,
                "id": str(self._client_id),
            },
            "timestamp": self._timestamp, 
            "version": {
                "major": self._major_version,
                "minor": self._minor_version,
                "revision": self._revision_version,
            },
        }

        if payload is not None:
            _data["size"] = len(self._payload)
            _data["payload"] = self._payload.python
        elif payload_size is not None:
            _data["size"] = payload_size

        super().__init__(_data)
    
    @classmethod
    def metadata(cls) -> Mapping[str, Union[str, int, float]]:
        dummy_packet= cls(
            "message_broker",
            uuid4(),
            0.0,
            "dummy payload",
        )
        return {
            "header_byte_size": len(dummy_packet.header()),
            "major_version": dummy_packet._major_version,
            "minor_version": dummy_packet._minor_version,
            "revision_version": dummy_packet._revision_version,
        }

    def add_payload(self, data: Any, is_json=False) -> None:
        if data is not None:
            if is_json:
                self._payload = _JSONEncodable(data, load=True)
            else:
                self._payload = _JSONEncodable(data)

            self["payload"] = self._payload.python
            self["size"] = len(self._payload)

    def header(self) -> str:
        """Get the packet header in JSON.

        Returns:
            str: The packet header as a JSON encoded string.
        """
        return str(_JSONEncodable({
            "client": {
                "type": "{:03d}".format(int(self["client"]["type"])),
                "id": str(self["client"]["id"]),
            },
            "size": "{:019d}".format(int(self["size"])),
            "timestamp": "{:019d}".format(int(self["timestamp"]*(10**6))), 
            "version": {
                "major": "{:03d}".format(int(self["version"]["major"])),
                "minor": "{:03d}".format(int(self["version"]["minor"])),
                "revision": "{:03d}".format(int(self["version"]["revision"])),
            }
        }))

    def payload(self) -> str:
        """Get the payload in JSON.

        Returns:
            str: The payload string as a JSON encoded string.
        """
        return str(self._payload.json)

    def send_over(self, conn: socket) -> None:
        """
        Sends the packet over the socket supplied by conn
        """
        if self["size"] == 0:
            conn.sendall(bytes(self.header(),"utf-8"))
        else:
            conn.sendall(bytes(self.header(),"utf-8"))
            conn.sendall(bytes(self.payload(),"utf-8"))

    @classmethod
    def from_socket(cls, conn: socket) -> Packet:
        msg = conn.recv(Packet.metadata()["header_byte_size"])
        # print("TRACE:: {}".format(msg), file=sys.stderr)
        packet = cls.from_json(
            msg,
        )
        # print("TRACE:: {}".format(dict(packet)), file=sys.stderr)
        if packet["size"] > 0:
            packet.add_payload(
                conn.recv(packet["size"]),
                is_json=True,
            )
        return packet
        
    @classmethod
    def from_json(cls, header: Union[str, bytes], payload: Union[str, bytes]=None) -> Packet:
        header = _JSONEncodable(header, load=True).native()
        if payload is not None:
            payload = _JSONEncodable(payload, load=True)
        return cls(
            int(header["client"]["type"]),
            UUID(header["client"]["id"]),
            float(header["timestamp"])/(10**6),
            payload=payload,
            payload_size=int(header["size"]),
            version=(
                int(header["version"]["major"]),
                int(header["version"]["minor"]),
                int(header["version"]["revision"]),
            )
        )

class Server:
    """Generic Class used for classification.
    """
    def __init__(self) -> None:
        raise NotImplementedError("'sap.Server' is not a concrete class")

class MessageBroker(Server):
    def __init__(
        self,
        topics: Iterable[str],
        addr: IPv4Interface=IPv4Interface("127.0.0.1/8"),
        port: int=65432,
        timeout=5.0,
    ) -> None:
        self.timeout = timeout
        self.socket = create_server((
            addr.ip.exploded,
            port,
        ))
        # For now disable blocking to prevent hangups
        self.socket.setblocking(False)

        self.topics = {t:{"subs": {}, "pubs":{}, "lock": RLock()} for t in topics}
        """
        self.topics breakdown

        .. code-block:: python
        {
            "[topic_string]": {
                "lock": multiprocessing.RLock(),
                "pubs": {
                    "[client_id]": {
                            "conn": socket.Socket(),
                            "addr": Tuple[str, int],
                            "lock": multiprocessing.RLock(),
                    },
                },
                "subs": {
                    "[client_id]": {
                            "conn": socket.Socket(),
                            "addr": Tuple[str, int],
                            "lock": multiprocessing.RLock(),
                    },
                },
            }
        }
        """
        self.time_to_quit = False

        self._start()
        print("TRACE:: Message Broker Finished", file=sys.stderr)

    def _quit(self, *args) -> None:
        self.time_to_quit = True

    def _broker_topic(self, topic: str) -> None:
        """
        K.I.S.S. for now; optimize later
            For each publisher check for a message
            For each recieved message forward to every subscriber
        """
        while not self.time_to_quit:
            pub_clients = None
            sub_clients = None
            with self.topics[topic]["lock"]:
                pub_clients = {client_id: pub for client_id, pub in self.topics[topic]["pubs"].items()}
                sub_clients = {client_id: sub for client_id, sub in self.topics[topic]["subs"].items()}
            if pub_clients is None or sub_clients is None:
                continue

            packets = []
            
            # Retrieve at most 1 message per Pub Client
            for client_id in pub_clients:
                with pub_clients[client_id]["lock"]:
                    try:
                        conn = pub_clients[client_id]["conn"]
                        packet = {
                            "header": conn.recv(HEADER_META["size"]).decode()
                        }
                        decoded_header = decode_packet_header(packet["header"])
                        packet["payload"] = conn.recv(decoded_header["size"]).decode()
                        packets.append(packet)
                    except:
                        # For now just terminate & remove the connection.
                        print("Unhandled error in MessageBroker._broker_topic('{}'):\n\t{}".format(topic, format_exc().replace("\n","\n\t")), file=sys.stderr)
                        Thread(
                            target=self._remove_client,
                            args=(
                                client_id,
                                "pubs",
                                topic,
                            ),
                        ).start()

            # Forward Message(s) to all Sub Clients
            if len(packets) > 0:
                for client_id in sub_clients:
                    with sub_clients[client_id]["lock"]:
                        for packet in packets:
                            try:
                                # print("MessageBroker::_broker_topic::TRACE::'{}'".format(client_id), file=sys.stderr)
                                conn=sub_clients[client_id]["conn"]
                                conn.sendall(packet["header"].encode())
                                conn.sendall(packet["payload"].encode())
                            except Exception as e:
                                # For now just terminate & remove the connection.
                                print("Removing Subscribing Client '{c}' due to Socket Error: '{e}'".format(c=client_id, e=e))
                                Thread(
                                    target=self._remove_client,
                                    args=(
                                        client_id,
                                        "subs",
                                        topic,
                                    ),
                                ).start()

    def _remove_client(self, client_id: str, type_key: str, topic: str) -> None:
        if type_key not in ("pubs", "subs"):
            raise ValueError("type_key", type_key)

        print("Cleaning up connection of '{}' client '{}'".format(type_key, client_id), file=stderr)

        with self.topics[topic]["lock"]:
            if type_key == "pubs":
                if client_id in self.topics[topic]["pubs"]:
                    with self.topics[topic]["pubs"][client_id]["lock"]:
                        try:
                            self.topics[topic]["pubs"][client_id]["conn"].close()
                        except:
                            pass
                    self.topics[topic]["pubs"].pop(client_id)
            if type_key == "subs":
                if client_id in self.topics[topic]["subs"]:
                    with self.topics[topic]["subs"][client_id]["lock"]:
                        try:
                            self.topics[topic]["subs"][client_id]["conn"].close()
                        except:
                            pass
                    self.topics[topic]["subs"].pop(client_id)

    def _add_client(self, conn: socket, addr: Tuple[str, int], max_tries=5) -> None:
        # Put connection in timeout mode to on initial startup
        conn.settimeout(self.timeout)
        tries = 0
        never_connected = True
        while tries < max_tries and never_connected:
            try:
                never_connected = False
                first_packet = {
                    "header": conn.recv(HEADER_META["size"]).decode(),
                }
                first_decoded_header = decode_packet_header(first_packet["header"])
                client_type_int = first_decoded_header["client"]["type"]
                client_id_str = str(first_decoded_header["client"]["id"])
                if client_type_int == CLIENT_TYPES["subscriber"] or client_type_int == CLIENT_TYPES["publisher"]:
                    assert first_decoded_header["size"] == 0, "Initial Packet malformed: size > 0"
                    second_packet = {
                        "header": conn.recv(HEADER_META["size"]).decode(),
                    }
                    second_decoded_header = decode_packet_header(second_packet["header"])
                    second_packet["payload"] = conn.recv(second_decoded_header["size"]).decode()
                    assert second_decoded_header["client"]["id"] == first_decoded_header["client"]["id"], "Lastest Packet & Packet before have different Client IDs"
                    assert second_decoded_header["client"]["type"] == first_decoded_header["client"]["type"], "Client's Type Changed in-between packets"
                    assert second_decoded_header["size"] > 0, "Topic Packet is Malformed: Size is '{}'".format(second_decoded_header["size"])
                    assert "topic" in json.loads(second_packet["payload"]), "Topic Packet is Malformed: 'topic' key not found in payload."
                    client_topic = json.loads(second_packet["payload"])["topic"]
                    if client_topic not in self.topics:
                        raise InvalidTopic(client_topic, client_id_str)
                    self.topics[client_topic]["lock"].acquire()
                    try:
                        if client_type_int == CLIENT_TYPES["subscriber"]:
                            if client_id_str in self.topics[client_topic]["subs"]:
                                raise FatalError("Client '{}' already present; can't re add".format(client_id_str))
                            self.topics[client_topic]["subs"][client_id_str] = {
                                "conn": conn,
                                "addr": addr,
                                "lock": RLock(),
                            }
                        elif client_type_int == CLIENT_TYPES["publisher"]:
                            if client_id_str in self.topics[client_topic]["pubs"]:
                                raise FatalError("Client '{}' already present; can't re add".format(client_id_str))
                            self.topics[client_topic]["pubs"][client_id_str] = {
                                "conn": conn,
                                "addr": addr,
                                "lock": RLock(),
                            }
                    finally:
                        self.topics[client_topic]["lock"].release()
                else:
                    raise InvalidClientType(client_type_int, client_id_str)
            except socket_timeout:
                pass
            except FatalError as e:
                print("Terminating Broker due to FATAL ERROR: '{}'".format(e), file=sys.stderr)
                try:
                    conn.close()
                except:
                    pass
                self.time_to_quit = True
                break
            except (InvalidClientType, InvalidTopic) as e:
                print("Couldn't Add Client due to ERROR: '{}'".format(e), file=sys.stderr)
                try:
                    conn.close()
                except:
                    pass
                break
            except:
                print("Couldn't Add Client due to unhandled error in MessageBroker._add_client:\n\t{}".format(format_exc().replace("\n","\n\t")), file=sys.stderr)
                try:
                    conn.close()
                except:
                    pass
                break
            finally:
                tries += 0

    def _start(self) -> None:
        topic_handlers = {}
        for topic in self.topics:
            topic_handlers[topic] = Thread(
                target=self._broker_topic,
                args=(
                    topic,
                )
            )
            topic_handlers[topic].start()
        
        self.socket.listen()
        while not self.time_to_quit:
            try:
                # Wait for new connection
                conn, addr = self.socket.accept()
                # Handle the connection
                Thread(
                    target=self._add_client,
                    args=(
                        conn,
                        addr,
                    )
                ).start()
            except BlockingIOError:
                pass
            except:
                print("Unhandled error in MessageBroker._start:\n\t{}".format(format_exc().replace("\n","\n\t")), file=sys.stderr)
                self.time_to_quit = True

        print("Cleaning up Message Broker", file=sys.stderr)

        #Wait for all threads to quit as well
        for t in topic_handlers:
            print("{}".format(t), file=sys.stderr)
            topic_handlers[t].join()

        # Cleanup connections
        for topic in self.topics:
            with self.topics[topic]["lock"]:
                for client_id in self.topics[topic]["pubs"]:
                    with self.topics[topic]["pubs"][client_id]["lock"]:
                        try:
                            self.topics[topic]["pubs"][client_id]["conn"].close()
                        except:
                            pass
                        self.topics[topic]["pubs"].pop(client_id)
                for client_id in self.topics[topic]["subs"]:
                    with self.topics[topic]["subs"][client_id]["lock"]:
                        try:
                            self.topics[topic]["subs"][client_id]["conn"].close()
                        except:
                            pass
                        self.topics[topic]["subs"].pop(client_id)

class Client:
    """Generic Class used for classification.
    """
    def __init__(self) -> None:
        raise NotImplementedError("'sap.Client' is not a concrete class")

class Subscriber(Client):
    def __init__(
        self,
        topic: str,
        broker_addr: IPv4Address=IPv4Address("127.0.0.1"),
        broker_port: int=65432,
        id: UUID=None,
        timeout=5.0,
    ) -> None:
        assert isinstance(topic, str)
        assert isinstance(broker_addr, IPv4Address)
        assert isinstance(broker_port, int)
        assert isinstance(id, (type(None), UUID))
            
        self.topic = topic
        self.id = uuid4() if id is None else id
        assert isinstance(self.id, UUID)
        self.client_type = CLIENT_TYPES["subscriber"]
        self._messages = multiprocessing.Queue()
        self.time_to_quit = False

        self.conn = create_connection((broker_addr.exploded, int(broker_port)))
        self.conn.settimeout(timeout)
        self._connect()
        if self.time_to_quit:
            raise FatalError("Subscriber couldn't connect to Broker")
        self._main_thread = Thread(
            target=self._start
        )
        self._main_thread.start()

    def _quit(self, *args):
        self.time_to_quit = True

    def _connect(self) -> None:
        topic_payload = json.dumps({"topic": self.topic})
        packets = (
            {
                "header": json.dumps(packet_header(self.client_type, self.id, 0)),
                "payload": "",
            },
            {
                "header": json.dumps(packet_header(self.client_type, self.id, len(topic_payload.encode()))),
                "payload": topic_payload,
            },            
        )
        try:
            # Connection Message
            self.conn.sendall(packets[0]["header"].encode())
            # Topic Message
            self.conn.sendall(packets[1]["header"].encode())
            self.conn.sendall(packets[1]["payload"].encode())
        except:
            print("Subscriber couldn't connect to message broker due to ERROR:\n\t'{}'".format(format_exc().replace("\n","\n\t")), file=sys.stderr)
            try:
                self.conn.close()
            except:
                pass
            self.time_to_quit = True
    
    def _start(self) -> None:
        while not self.time_to_quit:
            try:
                packet = {
                    "header": self.conn.recv(HEADER_META["size"]).decode()
                }
                packet_header = decode_packet_header(packet["header"])
                packet["payload"] = self.conn.recv(packet_header["size"]).decode()
                # print("Subscriber::_start::TRACE::'{}'".format(pack), file=sys.stderr)
                self._messages.put(
                    {
                        "header": packet_header,
                        "payload": json.loads(packet["payload"]),
                    }
                )
            except socket_timeout:
                print("sap::Subscriber::_start::TRACE::'{}'".format(format_exc()), file=sys.stderr)
            except:
                print("Subscriber closing connection to message broker due to ERROR:\n\t{}".format(format_exc().replace("\n","\n\t")), file=sys.stderr)
                try:
                    self.conn.close()
                except:
                    pass
                self.time_to_quit = True
        
        try:
            self.conn.close()
        except:
            pass
        
    def get_message(self, timeout=1) -> Mapping:
        try:
            msg = self._messages.get(timeout=timeout)
            # print("Subscriber::get_message::TRACE::'{}'".format(msg), file=sys.stderr)
            return msg["payload"] #Already decoded into the original Python Map
        except queue.Empty as e:
            raise TimeoutError("No Message could be retrieved in '{}'s".format(timeout))
        except Exception as e:
            if self.time_to_quit:
                raise FatalError("Subscribing Client has already quit. No more messages will appear.")
            else:
                print("Unhandled error in Subscriber.get_message:\n\t{}".format(format_exc().replace("\n","\n\t")), file=sys.stderr)
                raise e

    def get_all_messages(self, max_len = 10, timeout=1) -> Iterable[Mapping]:
        try:
            output = []
            while not self._messages.empty() and len(output) < max_len:
                output.append(
                    self._messages.get(timeout=timeout)["payload"] #Already decoded into the original Python Map
                )
            return tuple(output) 
        except queue.Empty as e:
            raise TimeoutError("No Message could be retrieved in '{}'s".format(timeout))
        except Exception as e:
            if self.time_to_quit:
                raise FatalError("Subscribing Client has already quit. No more messages will appear.")
            else:
                print("Unhandled error in Subscriber.get_message:\n\t{}".format(format_exc().replace("\n","\n\t")), file=sys.stderr)
                raise e

class Publisher(Client):
    def __init__(
        self,
        topic: str,
        broker_addr: IPv4Address=IPv4Address("127.0.0.1"),
        broker_port: int=65432,
        id: UUID=None,
        timeout=5.0,
    ) -> None:
        assert isinstance(topic, str)
        assert isinstance(broker_addr, IPv4Address)
        assert isinstance(broker_port, int)
        assert isinstance(id, (type(None), UUID))
    
        self.topic = topic
        self.id = uuid4() if id is None else id
        assert isinstance(self.id, UUID)
        self.client_type = CLIENT_TYPES["publisher"]
        self._messages = multiprocessing.Queue()
        self.time_to_quit = False

        self.conn = create_connection((broker_addr.exploded, int(broker_port)))
        # For now disable blocking to prevent hangups
        self.conn.settimeout(timeout)
        self._connect()
        if self.time_to_quit:
            raise FatalError("Publisher couldn't connect to Broker")
        self._main_thread = Thread(
            target=self._start
        )
        self._main_thread.start()

    def _quit(self, *args):
        self.time_to_quit = True

    def _connect(self) -> None:
        topic_payload = json.dumps({"topic": self.topic})
        packets = (
            {
                "header": json.dumps(packet_header(self.client_type, self.id, 0)),
                "payload": "",
            },
            {
                "header": json.dumps(packet_header(self.client_type, self.id, len(topic_payload.encode()))),
                "payload": topic_payload,
            },            
        )
        try:
            # Connection Message
            self.conn.sendall(packets[0]["header"].encode())
            # Topic Message
            self.conn.sendall(packets[1]["header"].encode())
            self.conn.sendall(packets[1]["payload"].encode())
        except:
            print("Publisher couldn't connect to message broker due to ERROR:\n\t{}".format(format_exc().replace("\n","\n\t")), file=sys.stderr)
            try:
                self.conn.close()
            except:
                pass
            self.time_to_quit = True
    
    def _start(self) -> None:
        while not self.time_to_quit:
            try:
                packet = {
                    "payload": self._messages.get(), #retrieve JSON String
                }
                packet["header"] = json.dumps(packet_header(
                    self.client_type,
                    self.id,
                    len(packet["payload"].encode()),
                ))
                self.conn.sendall(packet["header"].encode())
                self.conn.sendall(packet["payload"].encode())
            except socket_timeout:
                print("sap::Subscriber::_start::TRACE::'{}'".format(format_exc()), file=sys.stderr)
            except:
                print("Publisher closing connection to message broker due to ERROR:\n\t{}".format(format_exc().replace("\n","\n\t")), file=sys.stderr)
                try:
                    self.conn.close()
                except:
                    pass
                self.time_to_quit = True
        try:
            self.conn.close()
        except:
            pass

    def send_message(self, data: Any, timeout=1.0) -> None:
        if self.time_to_quit:
            raise FatalError("Publisher has already quit, can't send new messages.")
        else:
            self._messages.put(
                json.dumps(data),
                timeout=timeout,
            )

    def send_many_messages(self, datas: Iterable, timeout=1.0) -> None:
        if self.time_to_quit:
            raise FatalError("Publisher has already quit, can't send new messages.")
        else:
            for data in datas:
                self._messages.put(
                    json.dumps(data),
                    timeout=timeout,
                )
