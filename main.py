from multiprocessing import  Process
from time import sleep
from traceback import format_exc
from typing import Mapping
from uuid import uuid4
import simulate
import visualize
import sap
import signal
import os
import json

def entrypoint(
    bodies, 
    total_simulation_time, 
    timestep_delta,
    sig_set,
) -> Mapping[str, Process]:
    topics = (
        "physics_update",
    )

    siml = simulate.multiprocess(
        bodies, 
        total_simulation_time, 
        timestep_delta,
    )
    
    brkr = {
        "process_object": Process(
            target=sap.MessageBroker,
            args=(
                topics,
            ),
            name="Broker_{}".format(uuid4()),
        )
    }

    vizl = {
        "process_object": Process(
            target=visualize.godot,
            args=(
                json.dumps({pm.id: pm.latest_state.expand() for pm in bodies}),
            ),
            name="Visualize_{}".format(uuid4()),
        )
    }

    return {
        "simulate": siml,
        "broker": brkr,
        "visualize": vizl,
    }

if __name__ == "__main__":
    time_to_quit = False

    def quit_app(*args):
        global time_to_quit 
        time_to_quit = True

    #Create 3 bodies
    bodies = (
        simulate.PointMass(
            {
                0: simulate.PhysicsState(
                    100,
                    simulate.Position(0,0,0),
                    simulate.Vector(0,0,0,1),
                    simulate.Vector(0,0,0,1),
                    (),
                )
            },
            0,
        ),
        simulate.PointMass(
            {
                0: simulate.PhysicsState(
                    1,
                    simulate.Position(0,10,0),
                    simulate.Vector(0,0,0,1),
                    simulate.Vector(0,0,0,1),
                    (),
                )
            },
            0,
        ),
        simulate.PointMass(
            {
                0: simulate.PhysicsState(
                    1,
                    simulate.Position(0,-10,0),
                    simulate.Vector(0,0,0,1),
                    simulate.Vector(0,0,0,1),
                    (),
                )
            },
            0,
        ),
        simulate.PointMass(
            {
                0: simulate.PhysicsState(
                    1,
                    simulate.Position(10,0,0),
                    simulate.Vector(0,0,0,1),
                    simulate.Vector(0,0,0,1),
                    (),
                )
            },
            0,
        ),
        simulate.PointMass(
            {
                0: simulate.PhysicsState(
                    1,
                    simulate.Position(-10,0,0),
                    simulate.Vector(0,0,0,1),
                    simulate.Vector(0,0,0,1),
                    (),
                )
            },
            0,
        ),
    )
    
    # exit(0)

    sig_set = (signal.SIGINT, signal.SIGTERM)

    procs = entrypoint(
        bodies,
        -1,
        1.0,
        sig_set,
    )

    for sig in sig_set:
        signal.signal(sig, quit_app)

    print("Starting IPC")
    procs["broker"]["process_object"].start()
    print("Starting Visualization")
    procs["visualize"]["process_object"].start()
    print("Starting Simulation")
    procs["simulate"]["process_object"].start()


    print('Press Ctrl+C to quit...\n')
    try:
        signal.sigwait(sig_set)
    except KeyboardInterrupt:
        pass
    print("\nQuiting...")

    for proc in procs.values():
        if proc["process_object"].pid is not None and proc["process_object"].is_alive():
            print("Sending SIGQUIT to {}".format(proc["process_object"].name))
            os.kill(proc["process_object"].pid, signal.SIGQUIT)
    
    for proc in procs.values():
        try:
            print("Waiting for {} to terminate".format(proc["process_object"].name))
            if proc["process_object"].pid is not None:
                proc["process_object"].join(10)
                if proc["process_object"].is_alive():
                    print("Killing {}".format(proc["process_object"].name))
                    proc["process_object"].kill()
        except:
            print(format_exc())

    print("fin")
