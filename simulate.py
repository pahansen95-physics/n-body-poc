from __future__ import annotations
from collections import UserDict
from multiprocessing.connection import Connection
from signal import signal, SIGTERM, SIGQUIT, SIGINT
import sys
from time import monotonic
from typing import Iterable, Mapping, Tuple, Union
from uuid import uuid4
from multiprocessing import Pipe, Process
from math import ceil
from copy import deepcopy
from sap import Publisher, FatalError
from traceback import format_exc

from pprint import pprint

class Vector(UserDict):
    def __init__(
        self,
        x: float,
        y: float,
        z: float,
        magnitude: float,
    ) -> None:
        assert isinstance(x, (float, int))
        assert isinstance(y, (float, int))
        assert isinstance(z, (float, int))
        assert isinstance(magnitude, (float, int))

        super().__init__(
            {
                "x": float(x),
                "y": float(y),
                "z": float(z),
                "mag": float(magnitude),
            }
        )
    
    def __repr__(self) -> str:
        return "{m}({x}, {y}, {z})".format(
            m=self["mag"],
            x=self["x"],
            y=self["y"],
            z=self["z"],
        )

    def __add__(self, other) -> Vector:
        return Vector(
            (self["mag"] * self["x"]) + (other["mag"] * other["x"]),
            (self["mag"] * self["y"]) + (other["mag"] * other["y"]),
            (self["mag"] * self["z"]) + (other["mag"] * other["z"]),
            1.0,
        )

    def __radd__(self, other) -> Vector:
        return self.__add__(other)

    def __mul__(self, other):
        #For Scalars only, use dot & cross for Vectors
        if isinstance(other, Vector):
            raise TypeError("You can't multiple two Vectors. Use the dot or cross product.")
        return Vector(
            (self["mag"] * self["x"]) * other,
            (self["mag"] * self["y"]) * other,
            (self["mag"] * self["z"]) * other,
            1.0,
        )

    def __rmul__(self, other):
        return self.__mul__(other)

    def __truediv__(self, other) -> Vector:
        #For Scalars only
        if isinstance(other, Vector):
            raise TypeError("You can't divide two Vectors.")
        return Vector(
            (self["mag"] * self["x"]) / other,
            (self["mag"] * self["y"]) / other,
            (self["mag"] * self["z"]) / other,
            1.0,
        )
    
    def __rtruediv__(self, other) -> Vector:
        return self.__truediv__(other)
    
    def unit(self) -> None:
        self["x"] *= self["mag"]
        self["y"] *= self["mag"]
        self["z"] *= self["mag"]
        self["mag"] = (
            (self["x"] ** 2) +
            (self["y"] ** 2) +
            (self["z"] ** 2)
        ) ** 0.5

    def expand(self) -> None:
        self["x"] *= self["mag"]
        self["y"] *= self["mag"]
        self["z"] *= self["mag"]
        self["mag"] = 1

    def copy(self) -> Vector:
        return Vector(
            self["x"],
            self["y"],
            self["z"],
            self["mag"],
        )

class Position(Vector):
    def __init__(
        self,
        x: float,
        y: float,
        z: float,
    ) -> None:
        assert isinstance(x, (float, int))
        assert isinstance(y, (float, int))
        assert isinstance(z, (float, int))
        super().__init__(
            float(x),
            float(y),
            float(z),
            1.0,
        )

    def __repr__(self) -> str:
        return "({x}, {y}, {z})".format(
            x=self["x"],
            y=self["y"],
            z=self["z"],
        )

    def __add__(self, other) -> Position:
        #For Vector Types only
        if not isinstance(other, Vector):
            raise TypeError("You can only add Vectors or other positions to a position.")
        if type(other) == Vector:
            return Position(
                self["x"] + (other["mag"] * other["x"]),
                self["y"] + (other["mag"] * other["y"]),
                self["z"] + (other["mag"] * other["z"]),
            )
        if type(other) == Position:
            return Position(
                self["x"] + other["x"],
                self["y"] + other["y"],
                self["z"] + other["z"],
            )
    
    def __radd__(self, other) -> Position:
        return self.__add__(other)

    def as_tuple(self) -> Tuple[float, float, float]:
        return (self["x"], self["y"], self["z"])

    def distance_from(self, p2: Position) -> float:
        return (
            (p2["x"] - self['x']) ** 2 +
            (p2["y"] - self['y']) ** 2 +
            (p2["z"] - self['z']) ** 2
        ) ** 0.5
    
    def vector_tail_to_head(self, head_position: Position) -> Vector:
        return Vector(
            (head_position["x"] - self['x']),
            (head_position["y"] - self['y']),
            (head_position["z"] - self['z']),
            1,
        )

class PhysicsState(UserDict):
    def __init__(
        self,
        mass: float,
        position: Position,
        velocity: Vector,
        acceleration: Vector,
        forces: Iterable[Vector],
    ) -> None:
        assert isinstance(mass, (float, int))
        assert isinstance(position, Position)
        assert isinstance(velocity, Vector)
        assert isinstance(acceleration, Vector)
        try:
            iter(forces)
        except:
            raise TypeError("Forces not Iterable")
        super().__init__(
            {
                "mass": float(mass),
                "pos": position,
                "vel": velocity,
                "acc": acceleration,
                "forces" : tuple(forces),
            }
        )

    def expand(self) -> Mapping[str, Union[Mapping, int, float]]:
        """Expands the Physics state into a nested Mapping of python primitaves.
        """
        output = {}
        for k,v in self.items():
            if k == "mass":
                output[k] = v
            elif k == "pos" or k == "vel" or k == "acc":
                output[k] = dict(v)
            elif k == "forces":
                output[k] = tuple([dict(f) for f in v])
        return output

    def copy(self) -> Mapping[str, Union[float, Iterable[Vector]]]:
        return deepcopy(self.data)

    def sum_forces(self) -> Vector:
        mega_force = Vector(0,0,0,1)
        for force in self["forces"]:
            mega_force += force
        return mega_force

class PointMass:
    def __init__(self, history: Mapping[float, PhysicsState], latest_time: float, id: Union[str, int]=None) -> None:
        self.states = history
        self.latest_time = latest_time
        self.latest_state = self.states[self.latest_time]
        self.id = str(uuid4()) if id is None else id

        self.forces = []

    def __repr__(self) -> str:
        return "id: {id}\nstates:{st}".format(
            id=self.id,
            st=self.states
        )

    def physics_step(self, timestep: float) -> None:
        #Apply the last state's forces & calculate the new accel, velocity & accel
        new_accel = self.latest_state["acc"] + (self.latest_state.sum_forces() / self.latest_state["mass"])
        new_vel = self.latest_state["vel"] + (self.latest_state["acc"] * timestep)
        new_pos = self.latest_state["pos"] + (self.latest_state["vel"] * timestep) + (0.5 * self.latest_state["acc"] * (timestep ** 2))

        #Create a new state w/ the current forces
        self.states[self.latest_time + timestep] = PhysicsState(
            self.latest_state["mass"],
            new_pos,
            new_vel,
            new_accel,
            tuple(self.forces),
        )

        #Update object
        self.latest_time += timestep
        self.latest_state = self.states[self.latest_time]
        self.forces = []


def calculate_gravitational_force(origin_body: PointMass, other_body: PointMass, G) -> Vector:
    # F=G*m1*m2/r^2
    # pprint("Numerator: {}".format(-1 * G * origin_body.latest_state["mass"] * other_body.latest_state["mass"]))
    # pprint("Denominator: {}".format(origin_body.latest_state["pos"].distance_from(other_body.latest_state["pos"]) ** 2))
    magnitude = (
        -1 * G * origin_body.latest_state["mass"] * other_body.latest_state["mass"] / 
        (
            origin_body.latest_state["pos"].distance_from(other_body.latest_state["pos"]) ** 2
        )
    )
    # pprint("Magnitude: {}".format(magnitude))
    v = other_body.latest_state["pos"].vector_tail_to_head(origin_body.latest_state["pos"]) * magnitude
    # v.unit()
    # pprint("Vector: {}".format(v))
    return v

def simulation_step(
    time_delta: float,
    bodies: Iterable[PointMass],
    G: float,
) -> None:
    for body1 in bodies:
        for body2 in bodies:
            if body1.id != body2.id:
                    gf = calculate_gravitational_force(
                        body1,
                        body2,
                        G,
                    )
                    body1.forces.append(gf)
    
    for body in bodies:
        body.physics_step(time_delta)

def simulate(
    bodies: Iterable[PointMass], 
    timedelta: float=1, 
    steps=-1,
    G: float=0.0000000000667408,
    update_fps: float=24.0,
    quit_sig_set = (SIGINT, SIGQUIT, SIGTERM),
):
    """[summary]

    Args:
        bodies (Iterable[PointMass]): [description]
        timedelta (float, optional): [description]. Defaults to 1.
        steps (int, optional): [description]. Defaults to -1.
        G (float, optional): [description]. Defaults to 0.0000000000667408.
        update_fps (float, optional): Maximum times per second the simulation should send a state update; rate may be slower due to simulation constraints. Defaults to 24.0.
        broker_addr (Tuple[str, int], optional): [description]. Defaults to ('127.0.0.1', 65432).
        quit_sig_set (tuple, optional): [description]. Defaults to (SIGINT, SIGQUIT, SIGTERM).

    Raises:
        ValueError: [description]

    Returns:
        [type]: [description]
    """
    time_to_quit = False
    
    def quit(*args):
        global time_to_quit
        time_to_quit=True
    for sig in quit_sig_set:
        signal(sig, quit)
    
    pub = Publisher(
        "physics_update",
    )

    #All units are assumed SI Standard base units
    #Simulation loop
    update_period = 1/update_fps
    update_datum = monotonic()
    steps_taken = 0
    while not time_to_quit:
        if steps == -1 or steps_taken < steps:
            simulation_step(timedelta, bodies, G)
            if monotonic() - update_datum >= update_period:
                try:
                    pub.send_message(
                        {body.id: body.latest_state.expand() for body in bodies},
                        timeout=0,
                    )
                except FatalError:
                    print(format_exc(), file=sys.stderr)
                    time_to_quit = True
                except:
                    print("Failed to send state update:\n\t{}".format(format_exc().replace("\n","\n\t")))
                update_datum = monotonic()
            steps_taken += 1
        else:
            print("all simulation steps completed")
            break

    print("quit after {} steps".format(steps_taken))

    return {body.id: body for body in bodies}

def multiprocess(
    sim_bodies: Iterable[PointMass],
    sim_time: float,
    time_step: float,
) -> Mapping[str, Union[Connection, Process]]:
    if sim_time < 0:
        steps = -1
    else:
        steps = ceil(sim_time / time_step)

    output= {
        "process_object": Process(
            target=simulate,
            args=(
                sim_bodies,
            ),
            kwargs={
                "timedelta": time_step,
                "steps": steps,
            },
            name="Simulate_{}".format(uuid4()),
        )
    }

    return output

if __name__ == "__main__":
    from math import ceil
    seconds = 3600
    delta = 0.1
    steps = int(
        ceil(
            seconds/delta
        )
    )
    print(
        "Simulating {se} seconds w/ a delta of {de} seconds in {st} steps".format(
            se=seconds,
            de=delta,
            st=steps,
        )
    )
    #Create 3 bodies
    bodies = (
        PointMass(
            {
                0: PhysicsState(
                    1,
                    Position(0,1,0),
                    Vector(0,0,0,1),
                    Vector(0,0,0,1),
                    (),
                )
            },
            0,
        ),
        PointMass(
            {
                0: PhysicsState(
                    1,
                    Position(1,0,0),
                    Vector(0,0,0,1),
                    Vector(0,0,0,1),
                    (),
                )
            },
            0,
        ),
        PointMass(
            {
                0: PhysicsState(
                    1,
                    Position(-1,0,0),
                    Vector(0,0,0,1),
                    Vector(0,0,0,1),
                    (),
                )
            },
            0,
        ),
    )
    body_results = simulate(bodies, 1.0, steps)
    pprint(tuple(tuple(body_results.values())[0].states.values())[-1])